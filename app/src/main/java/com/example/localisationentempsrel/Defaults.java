package com.example.localisationentempsrel;

public class Defaults {
    public final static String PUBLISH_KEY = "pub-c-b09703bf-d918-41e9-b9d4-0da5806c7c96";
    public final static String SUBSCRIBE_KEY = "sub-c-ef32aa34-c6b1-11e9-9b51-8ae91c2a8a9f";
    public final static String CHANNEL_NAME = "drivers_location";

}
